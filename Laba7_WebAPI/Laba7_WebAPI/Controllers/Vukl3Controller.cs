﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Laba7_WebAPI;
using Laba7_WebAPI.Models;
using Teachers;

namespace Laba7_WebAPI.Controllers
{
    public class Vukl3Controller : ApiController
    {
        private DBModel db = new DBModel();

        // GET: api/Vukl3
        public IQueryable<Vukl3> GetVukl3()
        {
            return db.Vukl3;
        }

        // GET: api/Vukl3/5
        [ResponseType(typeof(Vukl3))]
        public IHttpActionResult GetVukl3(int id)
        {
            Vukl3 vukl3 = db.Vukl3.Find(id);
            if (vukl3 == null)
            {
                return NotFound();
            }

            return Ok(vukl3);
        }

        // PUT: api/Vukl3/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutVukl3(int id, Vukl3 vukl3)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != vukl3.VuklID)
            {
                return BadRequest();
            }

            db.Entry(vukl3).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!Vukl3Exists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Vukl3
        [ResponseType(typeof(Vukl3))]
        public IHttpActionResult PostVukl3(Vukl3 vukl3)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Vukl3.Add(vukl3);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = vukl3.VuklID }, vukl3);
        }

        // DELETE: api/Vukl3/5
        [ResponseType(typeof(Vukl3))]
        public IHttpActionResult DeleteVukl3(int id)
        {
            Vukl3 vukl3 = db.Vukl3.Find(id);
            if (vukl3 == null)
            {
                return NotFound();
            }

            db.Vukl3.Remove(vukl3);
            db.SaveChanges();

            return Ok(vukl3);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool Vukl3Exists(int id)
        {
            return db.Vukl3.Count(e => e.VuklID == id) > 0;
        }

        [HttpGet]
        [Route("api/helloworld", Name = "helloworld")]
        public string helloworld() 
        {
            TeacherRepo objRepo = new TeacherRepo();
            string sayHello = objRepo.GetSomeFromTeacher();
            return sayHello;
        }
    }
}