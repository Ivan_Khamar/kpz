﻿using Exam_Test_2.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Windows;

namespace Exam_Test_2_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ShowStudents();

            dataGrid.Visibility = Visibility.Hidden;
            //dataGrid.Columns[0].Visibility = Visibility.Hidden;
            //dataGrid.Columns[3].Width = 56;
        }

        private HttpClient GlobalConnect()
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("https://localhost:44306/");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return httpClient;
        }
        private void ShowStudents()
        {
            HttpClient httpClient = GlobalConnect();

            HttpResponseMessage response = httpClient.GetAsync("api/Students").Result;
            if (response.IsSuccessStatusCode)
            {
                var students = response.Content.ReadAsAsync<IEnumerable<Students>>().Result;
                dataGrid.ItemsSource = students;

            }
            else
            {
                MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
        }

        private void showData(object sender, RoutedEventArgs e)
        {
            ShowStudents();
            dataGrid.Visibility = Visibility.Visible;
            dataGrid.Columns[0].Visibility = Visibility.Hidden;
            dataGrid.Columns[3].Width = 60;
            //dataGrid.Width = 290;
        }

        private void addDataButton_Click(object sender, RoutedEventArgs e)
        {
            HttpClient httpClient = GlobalConnect();

            var stud = new Students();
            stud.Name = nameTextBox.Text;
            stud.Surname = surnameTextBox.Text;
            stud.BirthDate = Convert.ToDateTime(birthDateTextBox.Text);//DateTime.Parse(birthDateTextBox.Text);//
            stud.StudentCard = studentCardTextBox.Text;

            var response = httpClient.PostAsJsonAsync("api/Students", stud).Result;

            if (response.IsSuccessStatusCode)
            {
                ShowStudents();
                dataGrid.Columns[0].Visibility = Visibility.Hidden;
                dataGrid.Columns[3].Width = 60;
            }
            else
            {
                MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
        }

        private void updateDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedItem != null)
            {
                HttpClient httpClient = GlobalConnect();

                Students stud = dataGrid.SelectedItem as Students;
                stud.Name = nameTextBox.Text;
                stud.Surname = surnameTextBox.Text;
                stud.BirthDate = Convert.ToDateTime(birthDateTextBox.Text);//DateTime.ParseExact(birthDateTextBox.Text, "yyyy-MM-dd HH:mm:ss tt", null);//DateTime.Parse(birthDateTextBox.Text);
                stud.StudentCard = studentCardTextBox.Text;

                var id = stud.StudentID;
                var url = "api/Students/" + id;
                HttpResponseMessage response = httpClient.PutAsJsonAsync(url, stud).Result;
                {
                    ShowStudents();
                    dataGrid.Columns[0].Visibility = Visibility.Hidden;
                    dataGrid.Columns[3].Width = 60;
                }
            }
        }

        private void deleteDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < dataGrid.SelectedItems.Count; i++)
                {
                    HttpClient httpClient = GlobalConnect();

                    Students stud = dataGrid.SelectedItems[i] as Students;
                    var id = stud.StudentID;
                    var url = "api/Students/" + id;

                    HttpResponseMessage response = httpClient.DeleteAsync(url).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        ShowStudents();
                        dataGrid.Columns[0].Visibility = Visibility.Hidden;
                        dataGrid.Columns[3].Width = 60;
                    }
                    else
                    {
                        MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                    }
                }
            }
        }

        private void selectStudentsButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < dataGrid.SelectedItems.Count; i++)
                {
                    //HttpClient httpClient = GlobalConnect();

                    Students stud = dataGrid.SelectedItems[i] as Students;
                    //var id = stud.StudentID;
                    //var url = "api/Students/" + id;

                    //HttpResponseMessage response = httpClient.DeleteAsync(url).Result;
                    //if (response.IsSuccessStatusCode)
                    {
                        //ShowStudents();
                        dataGrid.Columns[0].Visibility = Visibility.Hidden;
                        dataGrid.Columns[3].Width = 60;

                        nameTextBox.Text = stud.Name;
                        surnameTextBox.Text = stud.Surname;
                        birthDateTextBox.Text = stud.BirthDate.ToString();//DateTime.Parse(birthDateTextBox.Text);//
                        studentCardTextBox.Text = stud.StudentCard;
                    }
                    //else
                    //{
                        //MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                    //}
                }
            }
        }
    }
}
