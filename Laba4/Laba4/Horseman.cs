﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba4
{
    public abstract class Horseman : Warrior
    {
        public virtual void sayHello() 
        {
            Console.WriteLine("Horseman greetings.");
        }

        public int horsemanAge;

        public string horsemanRank;

        public string horsemanName;

        public string horseName;

        private string wifeSurname;

        protected string wifeName { get; set; }
        public abstract void ride();
        public virtual void getOnHorse()
        {
            Console.WriteLine("Horseman is ready.");
        }

        //void sayHello();

        //public sName(set;);
    }
}
