﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba4
{
    class Base
    {
        public Base()
        {
            Console.WriteLine("Base.Instance.Constructor");
            this.m_Field3 = new Tracker("Base.Instance.Field3");
            this.Virtual();
        }
        static Base()
        {
            Console.WriteLine("Base.Static.Constructor");
        }
        private Tracker m_Field1 = new Tracker("Base.Instance.Field1");
        private Tracker m_Field2 = new Tracker("Base.Instance.Field2");
        private Tracker m_Field3;
        static private Tracker s_Field1 = new Tracker("Base.Static.Field1");
        static private Tracker s_Field2 = new Tracker("Base.Static.Field2");
        virtual public void Virtual()
        {
            Console.WriteLine("Base.Instance.Virtual");
        }
    }
}
