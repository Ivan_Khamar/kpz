namespace Laba6_ORM_CodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Ekzamenu")]
    public partial class Ekzamenu
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int EkzamenID { get; set; }

        [Key]
        [Column(Order = 1)]
        public DateTime RezultsDate { get; set; }

        public DateTime? EkzDate { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int GroupID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AudID { get; set; }

        public int? VuklID { get; set; }
    }
}
