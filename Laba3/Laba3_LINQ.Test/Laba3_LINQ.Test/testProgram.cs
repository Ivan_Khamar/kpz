﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Laba3_LINQ.Test
{
    [TestClass]
    public class testProgram
    {
        [TestMethod]
        public void TestOwnExtention()
        {
            string sample = "Samppple";
            char charExample = 'p';

            Assert.AreEqual(3, Program.ownExtension(sample,charExample));
        }

        [TestMethod]
        public void TestQueryStringArray()
        {
            String[] boys = { "Charlie", "Johnny", "Chuck", "Bob Keed", "Bill Bert", "Travis", "Taylor", "Alan Ray", "Sam" };
            List<String> selectedBoys = new List<string>();
            selectedBoys.Add("Bob Keed");
            selectedBoys.Add("Bill Bert");
            selectedBoys.Add("Alan Ray");

            Assert.AreEqual(selectedBoys.Count, Program.QueryStringArray(boys).Count);
        }

        [TestMethod]
        public void TestMyConvertToArray()
        {
            String[] boys = { "Charlie", "Johnny", "Chuck", "Bob Keed", "Bill Bert", "Travis", "Taylor", "Alan Ray", "Sam" };
            List<String> selectedBoys = new List<string>();
            selectedBoys.Add("Charlie");
            selectedBoys.Add("Johnny");
            selectedBoys.Add("Chuck");
            selectedBoys.Add("Bob Keed");
            selectedBoys.Add("Bill Bert");
            selectedBoys.Add("Travis");
            selectedBoys.Add("Taylor");
            selectedBoys.Add("Alan Ray");
            selectedBoys.Add("Sam");

            int boysLenght = boys.Length;

            Assert.AreEqual(boysLenght, Program.myConvertToArray(selectedBoys.ToArray()).Length);
        }

        [TestMethod]
        public void TestMyIComparableMethod()
        {
            String[] xpected = {
                "Sam",
                "Chuck",
                "Johnny",
                "Travis",
                "Taylor",
                "Charlie",
                "Bob Keed",
                "Alan Ray",
                "Bill Bert" };
            
            String[] boys = { "Charlie", "Johnny", "Chuck", "Bob Keed", "Bill Bert", "Travis", "Taylor", "Alan Ray", "Sam" };

            IEnumerable<string> auto = Program.myIComparableMethod(boys);

            IEnumerable<string> auto2 = xpected.Cast<string>();

            Assert.AreEqual(auto2.Count(), auto.Count());
        }
    }
}
