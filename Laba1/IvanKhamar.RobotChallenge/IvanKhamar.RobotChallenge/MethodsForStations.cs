﻿using System.Collections.Generic;
using Robot.Common;

namespace IvanKhamar.RobotChallenge
{
    static public class MethodsForStations
    {
        static public Position closestStationSearch(Robot.Common.Robot robotKhamar, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation closestStation = null;
            int minimalWay = int.MaxValue;
            foreach (var station in map.Stations)
            {
                int minimalEnergyRequired = MethodsForEnergy.MinimalPassLenght(robotKhamar.Position, station.Position);
                if (IsNooneOnStation(station, robots) 
                    && MethodsForEnergy.IsEnergyReserveBiggerThanRequested(robotKhamar.Energy, minimalEnergyRequired))
                {
                    int distance = distanceHelper.FindDistance(station.Position, robotKhamar.Position);
                    if (distance < minimalWay)
                    {
                        minimalWay = distance;
                        closestStation = station;
                    }
                }
            }
            return closestStation == null ? null : closestStation.Position;
        }

        static public bool IsStationOccupied(Map map, Robot.Common.Robot generalRobot)
        {
            foreach (var station in map.Stations)
            {
                if (station.Position.Equals(generalRobot.Position))
                    return true;
            }
            return false;
        }

        static public bool IsNooneOnStation(EnergyStation station, IList<Robot.Common.Robot> robots)
        {
            return areNooneThere(station.Position, robots); 
        }
        static public bool areFoesChargingThere(EnergyStation station, Robot.Common.Robot robotKhamar, IList<Robot.Common.Robot> robots)
        {
            return areFoesThere(station.Position, robotKhamar, robots);
        }
        static public bool areNooneThere(Position cell, IList<Robot.Common.Robot> robots) 
        {
            foreach (var robot in robots)
            {
                {
                    if (robot.Position == cell)
                        return false;
                }
            }
            return true;
        }
        static public Position foesChargingSearch(Robot.Common.Robot robotKhamar, Map map, IList<Robot.Common.Robot> robots)
        {
            EnergyStation closestStation = null;
            int minimalWay = int.MaxValue;
            foreach (var station in map.Stations)
            {
                int minimalEnergyRequired = MethodsForEnergy.MinimalPassLenght(robotKhamar.Position, station.Position) + 30;
                if (areFoesChargingThere(station, robotKhamar, robots) && MethodsForEnergy.IsEnergyReserveBiggerThanRequested(robotKhamar.Energy, minimalEnergyRequired))
                {
                    int distance = distanceHelper.FindDistance(station.Position, robotKhamar.Position);
                    if (distance < minimalWay)
                    {
                        minimalWay = distance;
                        closestStation = station;
                    }
                }
            }
            return closestStation == null ? null : closestStation.Position;
        }
        static public bool areFoesThere(Position cell, Robot.Common.Robot robotKhamar, IList<Robot.Common.Robot> robots)
        {
            foreach (var robot in robots)
            {
                if (robot.OwnerName != robotKhamar.OwnerName)
                {
                    if (robot.Position == cell)
                        return true;
                }
            }
            return false;
        }
    }
}
