namespace Exam_Test.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Customers
    {
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [Column("Phone number")]
        [StringLength(50)]
        public string Phone_number { get; set; }

        [Required]
        [StringLength(50)]
        public string Password { get; set; }
    }
}
