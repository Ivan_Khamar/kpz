namespace Laba6_ORM_CodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Groups
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Groups()
        {
            MainTable = new HashSet<MainTable>();
        }

        [Key]
        public int GroupID { get; set; }

        [Required]
        [StringLength(30)]
        public string NameGroup { get; set; }

        public int Kyrs { get; set; }

        [Required]
        [StringLength(30)]
        public string Specialnist { get; set; }

        public int KilkistStud { get; set; }

        [Required]
        [StringLength(50)]
        public string Starosta { get; set; }

        public int VuklID { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MainTable> MainTable { get; set; }

        public virtual Vukl Vukl { get; set; }
    }
}
