﻿using Laba5.ViewModels;
using System.Collections.Generic;
using System.Media;
using System.Windows;
using System.Windows.Controls;

namespace Laba5.Views
{
    /// <summary>
    /// Interaction logic for ShellView.xaml
    /// </summary>
    public partial class ShellView : Window
    {
        public ShellView()
        {
            InitializeComponent();
        }

        private List<Warrior> LoadCollectionData()
        {
            List<Warrior> warriors = new List<Warrior>();
            warriors.Add(new Warrior()
            {
                Age = 21,
                Name = "George Douglass",
                
            });

            warriors.Add(new Warrior()
            {
                Age = 22,
                Name = "Francois Roche",
            });

            return warriors;
        }

        private void btnUpdate_Click(object sender, RoutedEventArgs e)
        {
            SystemSounds.Beep.Play();
            dAtaGrid1.ItemsSource = LoadCollectionData();
        }

        private void btnOpenAudioFile_Click(object sender, RoutedEventArgs e)
        {
            SoundPlayer player = new SoundPlayer(Properties.Resources.wavBeep);
            player.Play();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MyStateControl myState = new MyStateControl();
            bool sTat= myState.State;
            if ((bool)stateCheckBox.IsChecked) 
            {
                sTat = !sTat;
                MessageBox.Show((sTat).ToString()); 
            }
            if (!(bool)stateCheckBox.IsChecked) { MessageBox.Show((sTat).ToString()); }
        }

        private void txtValue_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
