namespace Laba6_ORM_CodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class MainTable_Copy
    {
        [Key]
        [Column(Order = 0)]
        public int EkzamID { get; set; }

        public DateTime? DataEkz { get; set; }

        [Key]
        [Column(Order = 1)]
        public TimeSpan ChasEkz { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int VuklID { get; set; }

        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PredmID { get; set; }

        [StringLength(20)]
        public string GroupID { get; set; }

        [Key]
        [Column(Order = 4)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AsistID { get; set; }

        [Key]
        [Column(Order = 5)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AudID { get; set; }
    }
}
