﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba4
{
    class frenchArcher : Archer
    {
        public int defaultSalaryInGoldIngots;
        public int actualSalaryInGoldIngots;
        public frenchArcher()
        {
            this.defaultSalary = 70;
            base.defaultSalary = 90;
        }

        public frenchArcher(double actualSalary) : base()
        {
            this.actualSalary = actualSalary;
            base.defaultSalary = actualSalary;
        }

        public frenchArcher(string ArcherName, double defaultSalary, double actualSalary)
        {
            this.defaultSalary = defaultSalary;
            this.actualSalary = actualSalary;
            this.ArcherName = ArcherName;
        }

        public double requestedSalary(double actualSalary, double defaultSalary)
        {
            return actualSalary + defaultSalary; 
        }

        public double requestedSalary(int actualSalaryInGoldIngots, int defaultSalaryInGoldIngots)
        {
            return actualSalaryInGoldIngots + defaultSalaryInGoldIngots;
        }
    }
}
