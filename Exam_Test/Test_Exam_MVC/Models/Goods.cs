namespace Test_Exam_MVC.Models
{
    using System.ComponentModel.DataAnnotations;

    public partial class Goods
    {
        public int GoodsID { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [StringLength(50)]
        public string GoodsName { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public int GoodsQuan { get; set; }
    }
}
