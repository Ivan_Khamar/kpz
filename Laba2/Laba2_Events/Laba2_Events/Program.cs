﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba2_Events
{
    class Program
    {
        static void Main(string[] args)
        {
            var video = new Video(){Title = "Video 1"};
            var video2 = new Video() { Title = "Video 2" };
            var videoEncoder = new VideoEncoder();      //створює події
            var mailService = new MailService();        //очікує на подію
            var messageService = new MessageService();  //очікує на подію

            videoEncoder.VideoEncoded += mailService.OnVideoEncoded;
            videoEncoder.VideoEncoded += messageService.OnVideoEncoded;

            videoEncoder.VideoEncoded2 += mailService.OnVideoEncoded;
            videoEncoder.VideoEncoded2 += messageService.OnVideoEncoded;

            videoEncoder.Encode(video, video2);
        }
    }
}
