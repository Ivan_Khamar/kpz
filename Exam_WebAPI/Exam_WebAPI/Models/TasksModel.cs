using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Exam_WebAPI.Models
{
    public partial class TasksModel : DbContext
    {
        public TasksModel()
            : base("name=TasksModel")
        {
        }

        public virtual DbSet<Tasks> Tasks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tasks>()
                .Property(e => e.TaskText)
                .IsUnicode(false);

            modelBuilder.Entity<Tasks>()
                .Property(e => e.TaskTime)
                .IsUnicode(false);
        }
    }
}
