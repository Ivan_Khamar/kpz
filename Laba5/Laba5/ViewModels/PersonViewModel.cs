﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Laba5.Models;

namespace Laba5.ViewModels
{
    public class PersonViewModel
    {
        private IList<Person> _personList;
        public PersonViewModel()
        {
            _personList = new List<Person>()
        {
            new Person(){Name="Claude", Address="Peugeout"},
            new Person(){Name="John",Address="York"}
        };
        }
        public IList<Person> Persons
        {
            get { return _personList; }
            set { _personList = value; }
        }
        private ICommand mUpdater;
        public ICommand UpdateCommand
        {
            get
            {
                if (mUpdater == null)
                    mUpdater = new Updater();
                return mUpdater;
            }
            set
            {
                mUpdater = value;
            }
        }
    }
    class Updater : ICommand
    {
        #region ICommand Members  

        public bool CanExecute(object parameter)
        {
            return true;
        }
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            // код тут  
            MessageBox.Show("Update execution.");
        }
        #endregion
    }
}
