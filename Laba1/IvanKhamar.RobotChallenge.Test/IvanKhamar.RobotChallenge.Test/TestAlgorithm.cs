﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;

namespace IvanKhamar.RobotChallenge.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestMoveCommand()
        {
            var algorithm = new IvanKhamarAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 200, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(2,3) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is MoveCommand);
            Assert.AreEqual(((MoveCommand)command).NewPosition, stationPosition);
        }

        [TestMethod]
        public void TestCollectCommand()
        {
            var algorithm = new IvanKhamarAlgorithm();
            var map = new Map();
            var stationPosition = new Position(1, 1);
            map.Stations.Add(new EnergyStation() { Energy = 200, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(1,1) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void TestCreateNewRobotCommand()
        {
            var algorithm = new IvanKhamarAlgorithm();
            var map = new Map();
            var stationPosition = new Position(4, 2);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 600, Position = new Position(7,6) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void TestStationSearchNeedsMap()
        {
            var algorithm = new IvanKhamarAlgorithm();
            var map = new Map();
            //var stationPosition = new Position(4, 2);
            //map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 600, Position = new Position(7,6) } };

            Robot.Common.Robot robotKhamar = robots[0];
            Position CoordsForStation;
            bool isOccupied = true;
            CoordsForStation = MethodsForStations.foesChargingSearch(robotKhamar, map, robots);

            if (CoordsForStation == null)
            {
                isOccupied = false;
                CoordsForStation = MethodsForStations.closestStationSearch(robotKhamar, map, robots);
            }

            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(isOccupied is false);
            Assert.AreEqual(null, CoordsForStation);
        }

        [TestMethod]
        public void TestIsIsStationOccupiedWorking()
        {
            var algorithm = new IvanKhamarAlgorithm();
            var map = new Map();
            var station1Position = new Position(4, 2);
            var station2Position = new Position(8, 9);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = station1Position, RecoveryRate = 5 });
            map.Stations.Add(new EnergyStation() { Energy = 200, Position = station2Position, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(8,9) } };
            var command = algorithm.DoStep(robots, 0, map);

            Assert.IsTrue(command is CollectEnergyCommand);
            Assert.AreEqual(MethodsForStations.IsStationOccupied(map, robots[0]), true);
        }

        [TestMethod]
        public void TestMinimalPassLength()
        {
            var algorithm = new IvanKhamarAlgorithm();
            var map = new Map();
            var stationPosition = new Position(4, 2);
            map.Stations.Add(new EnergyStation() { Energy = 100, Position = stationPosition, RecoveryRate = 2 });
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 600, Position = new Position(2,2) } };

            int energyForActNeeded = int.MaxValue;
            bool isOccupied = true;
            if (stationPosition != null)
            {
                energyForActNeeded = MethodsForEnergy.MinimalPassLenght(robots[0].Position, stationPosition);
                if (isOccupied)
                    energyForActNeeded += 30;
            }

            var command = algorithm.DoStep(robots, 0, map);

            Assert.AreEqual(energyForActNeeded,34);
        }

        [TestMethod]
        public void TestIsEnergyReserveBiggerThanRequested()
        {
            var algorithm = new IvanKhamarAlgorithm();
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(7,6) } };
            MethodsForEnergy metods;
            int newRobotEnergyResourse = 10;
            bool isEnoughEnergy = true;
            bool rezult = MethodsForEnergy.IsEnergyReserveBiggerThanRequested(robots[0].Energy, (200 + newRobotEnergyResourse) );

            Assert.AreNotEqual(isEnoughEnergy, rezult);
        }

        [TestMethod]
        public void TestAreFoesThere()
        {
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(7,6) } };
            robots.Add(new Robot.Common.Robot() { Energy = 300, Position = new Position(2, 3), OwnerName = "Khamar Ivan"});
            robots.Add(new Robot.Common.Robot() { Energy = 100, Position = new Position(4, 1), OwnerName = "Kolisnuk Vasyl" });
            
            var Cell = new Position() {X = 5, Y = 3};
            bool rezult = IvanKhamar.RobotChallenge.MethodsForStations.areFoesThere(Cell, robots[1], robots);
            
            Assert.IsFalse(rezult);
        }

        [TestMethod]
        public void TestAreNooneThere()
        {
            var robots = new List<Robot.Common.Robot>()
                {new Robot.Common.Robot() {Energy = 200, Position = new Position(7,6) } };
            robots.Add(new Robot.Common.Robot() { Energy = 300, Position = new Position(2, 3), OwnerName = "Khamar Ivan" });
            robots.Add(new Robot.Common.Robot() { Energy = 100, Position = new Position(4, 1), OwnerName = "Kolisnuk Vasyl" });

            var Cell = new Position() { X = 5, Y = 3 };
            bool rezult = IvanKhamar.RobotChallenge.MethodsForStations.areNooneThere(Cell, robots);

            Assert.IsTrue(rezult);
        }
    }
}
