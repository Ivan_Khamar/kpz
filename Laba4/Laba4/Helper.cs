﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba4
{
    enum trainedLevel
    {
        Low,
        Medium,
        High
    }
    class Helper
    {
        static public bool arePresent;

        protected int helperAge;
        public string helperName;
        public trainedLevel helperTraining;

        private Jacket jacket1;

        public Helper()
        {
            jacket1 = new Jacket(this);
        }
        void Foo() { }
        class helperOutfit { }

        static Helper() 
        {
            arePresent = true;
        }
    }
}
