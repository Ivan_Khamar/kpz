﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace IvanKhamar.RobotChallenge
{
    public class IvanKhamarAlgorithm : IRobotAlgorithm
    {
        public string Author
        {
            get { return "Khamar Ivan"; }
        }

        public string Description
        {
            get { return "My final attempt for robots."; }
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot robotKhamar = robots[robotToMoveIndex];
            Position CoordsForStation;
            bool isOccupied = true;
            CoordsForStation = MethodsForStations.foesChargingSearch(robotKhamar, map, robots);

            if (CoordsForStation == null)
            {
                isOccupied = false;
                CoordsForStation = MethodsForStations.closestStationSearch(robotKhamar, map, robots);
            }
            int energyForActNeeded = int.MaxValue;
            if (CoordsForStation != null)
            {
                energyForActNeeded = MethodsForEnergy.MinimalPassLenght(robotKhamar.Position, CoordsForStation);
                if (isOccupied)
                    energyForActNeeded += 30;
            }
            if (MethodsForEnergy.IsEnoughEnergyForRobotCreation(energyForActNeeded, robotKhamar))
            {

                if (CoordsForStation != null)
                {
                    return new CreateNewRobotCommand() { NewRobotEnergy = energyForActNeeded };
                }
                else
                {
                    return new CollectEnergyCommand();
                }
            }
            else if (MethodsForStations.IsStationOccupied(map, robotKhamar))
            {
                return new CollectEnergyCommand();
            }
            else
            {

                if (CoordsForStation != null)
                {
                    if (MethodsForPosition.IsMoreMovesNeeded())
                    {
                        return new MoveCommand() { NewPosition = CoordsForStation };
                    }
                    else
                    {
                        MethodsForPosition.changePosition();

                    }
                }
            }
            return new CollectEnergyCommand();
        }
    }
}
