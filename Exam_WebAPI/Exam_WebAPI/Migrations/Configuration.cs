namespace Exam_WebAPI.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Exam_WebAPI.Models;

    internal sealed class Configuration : DbMigrationsConfiguration<Exam_WebAPI.Models.TasksModel>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Exam_WebAPI.Models.TasksModel context)
        {
            //  This method will be called after migrating to the latest version.
            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            context.Tasks.AddOrUpdate(x => x.TaskID,
            new Tasks() { TaskText = "Wake up", TaskTime = "ten minutes", TaskPriority = 2 },
            new Tasks() { TaskText = "Become ready", TaskTime = "one hour", TaskPriority = 3 },
            new Tasks() { TaskText = "Pass KPZ exam", TaskTime = "three hours", TaskPriority = 1 }
            );
        }
    }
}
