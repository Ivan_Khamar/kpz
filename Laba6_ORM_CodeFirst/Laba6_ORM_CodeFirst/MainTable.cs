namespace Laba6_ORM_CodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MainTable")]
    public partial class MainTable
    {
        [Key]
        public int EkzamID { get; set; }

        public DateTime? DataEkz { get; set; }

        public DateTime? ChasEkz { get; set; }

        public int VuklID { get; set; }

        public int PredmID { get; set; }

        public int GroupID { get; set; }

        public int AsistID { get; set; }

        public int AudID { get; set; }

        public virtual Aud Aud { get; set; }

        public virtual Groups Groups { get; set; }

        public virtual Predm Predm { get; set; }

        public virtual Vukl Vukl { get; set; }
    }
}
