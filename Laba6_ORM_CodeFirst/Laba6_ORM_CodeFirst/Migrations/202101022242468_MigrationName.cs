﻿namespace Laba6_ORM_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationName : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Aud_Migr",
                c => new
                    {
                        AudID = c.Int(nullable: false),
                        NameAud = c.String(nullable: false, maxLength: 50, unicode: false),
                        NumberAud = c.Int(nullable: false),
                        Korpys = c.Int(nullable: false),
                        Mistkist = c.Int(nullable: false),
                        RozmirAud = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AudID);
            
            CreateTable(
                "dbo.MainTable_Migr",
                c => new
                    {
                        EkzamID = c.Int(nullable: false, identity: true),
                        DataEkz = c.DateTime(),
                        ChasEkz = c.DateTime(),
                        VuklID = c.Int(nullable: false),
                        PredmID = c.Int(nullable: false),
                        GroupID = c.Int(nullable: false),
                        AsistID = c.Int(nullable: false),
                        AudID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EkzamID)
                .ForeignKey("dbo.Groups", t => t.GroupID)
                .ForeignKey("dbo.Vukl", t => t.VuklID)
                .ForeignKey("dbo.Predm", t => t.PredmID)
                .ForeignKey("dbo.Aud", t => t.AudID)
                .Index(t => t.VuklID)
                .Index(t => t.PredmID)
                .Index(t => t.GroupID)
                .Index(t => t.AudID);
            
            CreateTable(
                "dbo.Groups_Migr",
                c => new
                    {
                        GroupID = c.Int(nullable: false, identity: true),
                        NameGroup = c.String(nullable: false, maxLength: 30, unicode: false),
                        Kyrs = c.Int(nullable: false),
                        Specialnist = c.String(nullable: false, maxLength: 30, unicode: false),
                        KilkistStud = c.Int(nullable: false),
                        Starosta = c.String(nullable: false, maxLength: 50, unicode: false),
                        VuklID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.GroupID)
                .ForeignKey("dbo.Vukl", t => t.VuklID)
                .Index(t => t.VuklID);
            
            CreateTable(
                "dbo.Vukl_Migr",
                c => new
                    {
                        VuklID = c.Int(nullable: false, identity: true),
                        NSS = c.String(nullable: false, maxLength: 50, unicode: false),
                        Telef = c.String(nullable: false, maxLength: 20, unicode: false),
                        Specialnist = c.String(nullable: false, maxLength: 30, unicode: false),
                        Posada = c.String(nullable: false, maxLength: 30, unicode: false),
                    })
                .PrimaryKey(t => t.VuklID);
            
            CreateTable(
                "dbo.Predm_Migr",
                c => new
                    {
                        PredmID = c.Int(nullable: false, identity: true),
                        NamePredm = c.String(nullable: false, maxLength: 30, unicode: false),
                        KilkistGodun = c.Int(nullable: false),
                        PredmType = c.String(nullable: false, maxLength: 10, unicode: false),
                    })
                .PrimaryKey(t => t.PredmID);
            
            CreateTable(
                "dbo.Ekzamenu_Migr",
                c => new
                    {
                        EkzamenID = c.Int(nullable: false),
                        RezultsDate = c.DateTime(nullable: false),
                        GroupID = c.Int(nullable: false),
                        AudID = c.Int(nullable: false),
                        EkzDate = c.DateTime(),
                        VuklID = c.Int(),
                    })
                .PrimaryKey(t => new { t.EkzamenID, t.RezultsDate, t.GroupID, t.AudID });
            
            CreateTable(
                "dbo.MainTable_Copy_Migr",
                c => new
                    {
                        EkzamID = c.Int(nullable: false),
                        ChasEkz = c.Time(nullable: false, precision: 7),
                        VuklID = c.Int(nullable: false),
                        PredmID = c.Int(nullable: false),
                        AsistID = c.Int(nullable: false),
                        AudID = c.Int(nullable: false),
                        DataEkz = c.DateTime(),
                        GroupID = c.String(maxLength: 20, unicode: false),
                    })
                .PrimaryKey(t => new { t.EkzamID, t.ChasEkz, t.VuklID, t.PredmID, t.AsistID, t.AudID });
            
            CreateTable(
                "dbo.sysdiagrams_Migr",
                c => new
                    {
                        diagram_id = c.Int(nullable: false, identity: true),
                        name = c.String(nullable: false, maxLength: 128),
                        principal_id = c.Int(nullable: false),
                        version = c.Int(),
                        definition = c.Binary(),
                    })
                .PrimaryKey(t => t.diagram_id);
            
            CreateTable(
                "dbo.table1_Migr",
                c => new
                    {
                        i = c.Int(nullable: false),
                        col1 = c.String(nullable: false, maxLength: 20, unicode: false),
                        col2 = c.String(maxLength: 20, unicode: false),
                    })
                .PrimaryKey(t => t.i);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MainTable", "AudID", "dbo.Aud");
            DropForeignKey("dbo.MainTable", "PredmID", "dbo.Predm");
            DropForeignKey("dbo.MainTable", "VuklID", "dbo.Vukl");
            DropForeignKey("dbo.Groups", "VuklID", "dbo.Vukl");
            DropForeignKey("dbo.MainTable", "GroupID", "dbo.Groups");
            DropIndex("dbo.Groups", new[] { "VuklID" });
            DropIndex("dbo.MainTable", new[] { "AudID" });
            DropIndex("dbo.MainTable", new[] { "GroupID" });
            DropIndex("dbo.MainTable", new[] { "PredmID" });
            DropIndex("dbo.MainTable", new[] { "VuklID" });
            DropTable("dbo.table1");
            DropTable("dbo.sysdiagrams");
            DropTable("dbo.MainTable_Copy");
            DropTable("dbo.Ekzamenu");
            DropTable("dbo.Predm");
            DropTable("dbo.Vukl");
            DropTable("dbo.Groups");
            DropTable("dbo.MainTable");
            DropTable("dbo.Aud");
        }
    }
}
