﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using static Laba3_LINQ.SpecialComparer;

namespace Laba3_LINQ
{
    public static class StringExtension
    {
        public static int CharCount(this string str, char c)
        {
            int counter = 0;
            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == c)
                    counter++;
            }
            return counter;
        }
    }
    public class Word
    {
        public Dictionary<string, string> Langs;
    }
    public class Program
    {
        public class Trainee
        {
            public int TraineeID { get; set; }
            public string TraineeName { get; set; }
            public int age { get; set; }
        }
        static void Main(string[] args)
        {
            //int[] intArray = QueryIntArray(); //Отримуємо і друкуємо масив

            String[] boys = { "Charlie", "Johnny", "Chuck", "Bob Keed", "Bill Bert", "Travis", "Taylor", "Alan Ray", "Sam" };
            List<String> rezArray = QueryStringArray(boys); //1.Селекція частини інформації (метод Select)+ 2.Вибірка певної інформації (Where) + 8.Відсортувати список за ім’ям
            for (int i = 0; i < rezArray.Count; i++)
            {
                Console.WriteLine(QueryStringArray(boys)[i]);
            } ;

            Console.WriteLine('\n');

            dictionaryUse(); //3. Операції як з списком List так і з словником Dictionary

            int charIndex = ownExtension("Sample",'p'); //4.Реалізувати власні методи розширювання
            Console.WriteLine('\n');
            Console.WriteLine(charIndex);
                Console.WriteLine('\n');

            anonymousClass(); //5.Показати використання анонімних класів та ініціалізаторів

            myIComparableMethod(boys); //6.Відсортувати за якимось критерієм використовуючи шаблон IComparable.

            Console.WriteLine('\n');
            Console.WriteLine("Введіть ціну першої послуги: ");
            //decimal firstPrice = 1.2m;
            decimal firstPrice = Convert.ToDecimal(Console.ReadLine());
            Console.WriteLine("Введіть ціну другої послуги: ");
            //decimal secondPrice = 2.4m;
            decimal secondPrice = Convert.ToDecimal(Console.ReadLine());
            SpecialComparer myComparer = new SpecialComparer();
            int rezult = myComparer.Compare(firstPrice, secondPrice); //6.Відсортувати за якимось критерієм використовуючи шаблон IComparer.
            switch (rezult)
            {
                case 1:
                    Console.WriteLine("Значення "+firstPrice+ " більше значення " + secondPrice + ".\n");
                    break;
                case -1:
                    Console.WriteLine("Значення " + firstPrice + " менше " + secondPrice + ".\n");
                    break;
                case 0:
                    Console.WriteLine("Значення параметрів " + firstPrice + " і " + secondPrice + " рівні.\n");
                    break;
            }

            myConvertToArray(boys); //7.Конвертувати список в масив.

            Console.ReadLine();
        }

        public static List<String> QueryStringArray(String[] boys)
        {
            //String[] boys = { "Charlie", "Johnny", "Chuck", "Bob Keed", "Bill Bert", "Travis", "Taylor", "Alan Ray", "Sam"};

            var boySpaces = from boy in boys
                                                where boy.Contains(" ")
                                                orderby boy descending
                                                select boy;

            List<String> selected = new List<string>();

            foreach (var i in boySpaces)
            {
                selected.Add(i);
            }
            return selected;
        }

        public static void dictionaryUse()
        {
            List<string> langsToUse = new List<string> { "c#", "c++" };

            List<Word> wordsList = new List<Word> {
                new Word {
                    Langs = new Dictionary<string, string> {
                        {"c#", "static class"},
                        {"c++", "#define FOO"},
                    }},
                new Word {
                    Langs = new Dictionary<string, string> {
                        {"c#", "for each"},
                        {"c++", "for"},
                    }},
                new Word {
                    Langs = new Dictionary<string, string> {
                        {"c#", " #include"},
                        {"c++", "using"},
                        {"pascal", ""}
                    }},
            };

            //Вибрати всі слова за умови
            List<Word> validWords_2 = wordsList.Where(w => langsToUse.All(l => w.Langs[l] != "")).ToList();
            //validWords_2.ForEach(Console.WriteLine);

            foreach (var word in validWords_2)
            {
                //вивести елементи, якщо такі є 
                var langs = word.Langs.Where(l => langsToUse.Contains(l.Key));
                //друк в консоль
                Console.WriteLine("Difference is:");
                langs.ToList().ForEach(l => Console.WriteLine("{0} -> {1}", l.Key, l.Value));
            }
        }
        public static int ownExtension(string String, char Char)
        {
            //Console.WriteLine('\n');
            string s = String;
            char c = Char;
            int i = s.CharCount(c);
            //Console.WriteLine(i);
            //Console.WriteLine('\n');
            return i;
        }

        public static void anonymousClass()
        {
            IList<Trainee> traineeList = new List<Trainee>() {
                new Trainee() { TraineeID = 1, TraineeName = "Carol", age = 18 },  //анонімний ініціалізатор
                new Trainee() { TraineeID = 2, TraineeName = "Steve",  age = 21 }, //анонімний ініціалізатор
                new Trainee() { TraineeID = 3, TraineeName = "Lucy",  age = 18 },  //анонімний ініціалізатор
            };

            var trainees = from s in traineeList
                           select new { Id = s.TraineeID, Name = s.TraineeName };

            foreach (var train in trainees)
                Console.WriteLine(train.Id + "-" + train.Name);
        }

        public static IEnumerable<string> myIComparableMethod(String[] boys)
        {
            /*public static IOrderedEnumerable<T> OrderBy<T, K>(
                this IEnumerable<T> source,
                Func<T, K> keySelector)
                where
                К : IComparable<K>;*/

            //String[] boys = { "Charlie", "Johnny", "Chuck", "Bob Keed", "Bill Bert", "Travis", "Taylor", "Alan Ray", "Sam" };

            IEnumerable<string> auto = boys.OrderBy(s => s.Length);

            Console.WriteLine('\n');
            foreach (string str in auto)
                Console.WriteLine(str);

            return auto;
        }

        public static string[] myConvertToArray(String[] boys)
        {
            //String[] boys = { "Charlie", "Johnny", "Chuck", "Bob Keed", "Bill Bert", "Travis", "Taylor", "Alan Ray", "Sam" };

            Console.WriteLine('\n');
            //Console.WriteLine("Початковий тип: " + boys.ToString());

            string[] arr = boys.ToArray();

            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine("Пiсля застосування ToArray: " + arr[i]);
            }

            return arr;
        }
    }
}
