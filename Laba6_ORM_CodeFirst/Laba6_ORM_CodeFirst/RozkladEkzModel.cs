namespace Laba6_ORM_CodeFirst
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class RozkladEkzModel : DbContext
    {
        public RozkladEkzModel()
            : base("name=RozkladEkzModel_CodeFirst")
        {
        }

        public virtual DbSet<Aud> Aud { get; set; }
        public virtual DbSet<Groups> Groups { get; set; }
        public virtual DbSet<MainTable> MainTable { get; set; }
        public virtual DbSet<Predm> Predm { get; set; }
        public virtual DbSet<sysdiagrams> sysdiagrams { get; set; }
        public virtual DbSet<table1> table1 { get; set; }
        public virtual DbSet<Vukl> Vukl { get; set; }
        public virtual DbSet<Ekzamenu> Ekzamenu { get; set; }
        public virtual DbSet<MainTable_Copy> MainTable_Copy { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Aud>()
                .Property(e => e.NameAud)
                .IsUnicode(false);

            modelBuilder.Entity<Aud>()
                .HasMany(e => e.MainTable)
                .WithRequired(e => e.Aud)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Groups>()
                .Property(e => e.NameGroup)
                .IsUnicode(false);

            modelBuilder.Entity<Groups>()
                .Property(e => e.Specialnist)
                .IsUnicode(false);

            modelBuilder.Entity<Groups>()
                .Property(e => e.Starosta)
                .IsUnicode(false);

            modelBuilder.Entity<Groups>()
                .HasMany(e => e.MainTable)
                .WithRequired(e => e.Groups)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Predm>()
                .Property(e => e.NamePredm)
                .IsUnicode(false);

            modelBuilder.Entity<Predm>()
                .Property(e => e.PredmType)
                .IsUnicode(false);

            modelBuilder.Entity<Predm>()
                .HasMany(e => e.MainTable)
                .WithRequired(e => e.Predm)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<table1>()
                .Property(e => e.col1)
                .IsUnicode(false);

            modelBuilder.Entity<table1>()
                .Property(e => e.col2)
                .IsUnicode(false);

            modelBuilder.Entity<Vukl>()
                .Property(e => e.NSS)
                .IsUnicode(false);

            modelBuilder.Entity<Vukl>()
                .Property(e => e.Telef)
                .IsUnicode(false);

            modelBuilder.Entity<Vukl>()
                .Property(e => e.Specialnist)
                .IsUnicode(false);

            modelBuilder.Entity<Vukl>()
                .Property(e => e.Posada)
                .IsUnicode(false);

            modelBuilder.Entity<Vukl>()
                .HasMany(e => e.Groups)
                .WithRequired(e => e.Vukl)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Vukl>()
                .HasMany(e => e.MainTable)
                .WithRequired(e => e.Vukl)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MainTable_Copy>()
                .Property(e => e.GroupID)
                .IsUnicode(false);
        }
    }
}
