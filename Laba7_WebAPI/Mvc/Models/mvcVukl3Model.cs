﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Mvc.Models
{
    public class mvcVukl3Model
    {
        public int VuklID { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public string NSS { get; set; }
        public string Telef { get; set; }
        public string Specialnist { get; set; }
        public string Posada { get; set; }
    }
}