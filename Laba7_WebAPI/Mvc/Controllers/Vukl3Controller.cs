﻿using Mvc.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace Mvc.Controllers
{
    public class Vukl3Controller : Controller
    {
        // GET: Vukl3
        public ActionResult Index()
        {
            IEnumerable<mvcVukl3Model> vukl3List;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Vukl3").Result;
            vukl3List = response.Content.ReadAsAsync<IEnumerable<mvcVukl3Model>>().Result;
            return View(vukl3List);
        }
        public ActionResult AddOrEdit(int id = 0) 
        {
            if (id == 0)
                return View(new mvcVukl3Model());
            else
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Vukl3/"+id.ToString()).Result;
                return View(response.Content.ReadAsAsync<mvcVukl3Model>().Result);
            }
        }
        [HttpPost]
        public ActionResult AddOrEdit(mvcVukl3Model vkl)
        {
            if (vkl.VuklID == 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Vukl3", vkl).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Vukl3/"+vkl.VuklID, vkl).Result;
                TempData["SuccessMessage"] = "Updated Successfully";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Vukl3/"+id.ToString()).Result;
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Index");
        }
    }
}