﻿using Exam_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Exam_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            ShowTasks();
            dataGrid.Visibility = Visibility.Hidden;
        }
        private HttpClient GlobalConnect()
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri("https://localhost:44362/");
            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            return httpClient;
        }
        private void ShowTasks()
        {
            HttpClient httpClient = GlobalConnect();

            HttpResponseMessage response = httpClient.GetAsync("api/Tasks").Result;
            if (response.IsSuccessStatusCode)
            {
                var tasks = response.Content.ReadAsAsync<IEnumerable<Tasks>>().Result;
                dataGrid.ItemsSource = tasks;

            }
            else
            {
                MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
        }

        private void showData(object sender, RoutedEventArgs e)
        {
            ShowTasks();
            dataGrid.Visibility = Visibility.Visible;
            dataGrid.Columns[0].Visibility = Visibility.Hidden;
            //dataGrid.Columns[3].Width = 60;
        }

        private void selectStudentsButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < dataGrid.SelectedItems.Count; i++)
                {
                    //HttpClient httpClient = GlobalConnect();
                    Tasks task = dataGrid.SelectedItems[i] as Tasks;
                   
                    {
                        //ShowStudents();
                        dataGrid.Columns[0].Visibility = Visibility.Hidden;
                        //dataGrid.Columns[3].Width = 60;

                        textTextBox.Text = task.TaskText;
                        timeTextBox.Text = task.TaskTime;
                        priorityTextBox.Text = task.TaskPriority.ToString();
                    }
                }
            }
        }

        private void addDataButton_Click(object sender, RoutedEventArgs e)
        {
            HttpClient httpClient = GlobalConnect();

            var task = new Tasks();
            task.TaskText = textTextBox.Text;
            task.TaskTime = timeTextBox.Text;
            task.TaskPriority = int.Parse(priorityTextBox.Text);

            var response = httpClient.PostAsJsonAsync("api/Tasks", task).Result;

            if (response.IsSuccessStatusCode)
            {
                ShowTasks();
                dataGrid.Columns[0].Visibility = Visibility.Hidden;
                //dataGrid.Columns[3].Width = 60;
            }
            else
            {
                MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
            }
        }

        private void updateDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedItem != null)
            {
                HttpClient httpClient = GlobalConnect();

                Tasks task = dataGrid.SelectedItem as Tasks;
                task.TaskText = textTextBox.Text;
                task.TaskTime = timeTextBox.Text;
                task.TaskPriority = int.Parse(priorityTextBox.Text);

                var id = task.TaskID;
                var url = "api/Tasks/" + id;
                HttpResponseMessage response = httpClient.PutAsJsonAsync(url, task).Result;
                {
                    ShowTasks();
                    dataGrid.Columns[0].Visibility = Visibility.Hidden;
                    //dataGrid.Columns[3].Width = 60;
                }
            }
        }

        private void deleteDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedItems.Count > 0)
            {
                for (int i = 0; i < dataGrid.SelectedItems.Count; i++)
                {
                    HttpClient httpClient = GlobalConnect();

                    Tasks task = dataGrid.SelectedItems[i] as Tasks;
                    var id = task.TaskID;
                    var url = "api/Tasks/" + id;

                    HttpResponseMessage response = httpClient.DeleteAsync(url).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        ShowTasks();
                        dataGrid.Columns[0].Visibility = Visibility.Hidden;
                        //dataGrid.Columns[3].Width = 60;
                    }
                    else
                    {
                        MessageBox.Show("Error Code" + response.StatusCode + " : Message - " + response.ReasonPhrase);
                    }
                }
            }
        }
    }
}
