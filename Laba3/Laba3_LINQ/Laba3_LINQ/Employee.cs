﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba3_LINQ
{
    class Employee
    {
        public string Name { get; set; } 
        public double Weight { get; set; }
        public double Height { get; set; }
        public int AnimalID { get; set; }

        public Employee(string name = "No name",
            double weight = 0,
            double height = 0)
        {
            Name = name;
            Weight = weight;
            Height = height;
        }

        public override string ToString()
        {
            return string.Format("{0} weighs {1} kilogram and is {2} centimeters tall",
                Name, Weight, Height);
        }
    }
}
