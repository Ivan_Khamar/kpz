//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Laba6_ORM_DBFirst
{
    using System;
    using System.Collections.Generic;
    
    public partial class MainTable
    {
        public int EkzamID { get; set; }
        public Nullable<System.DateTime> DataEkz { get; set; }
        public Nullable<System.DateTime> ChasEkz { get; set; }
        public int VuklID { get; set; }
        public int PredmID { get; set; }
        public int GroupID { get; set; }
        public int AsistID { get; set; }
        public int AudID { get; set; }
    
        public virtual Aud Aud { get; set; }
        public virtual Groups Groups { get; set; }
        public virtual Predm Predm { get; set; }
        public virtual Vukl Vukl { get; set; }
    }
}
