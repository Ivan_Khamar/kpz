namespace Exam_WebAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Tasks
    {
        [Key]
        public int TaskID { get; set; }

        [Required]
        [StringLength(50)]
        public string TaskText { get; set; }

        [Required]
        [StringLength(50)]
        public string TaskTime { get; set; }

        public int TaskPriority { get; set; }
    }
}
