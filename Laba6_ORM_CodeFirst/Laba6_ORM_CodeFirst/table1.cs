namespace Laba6_ORM_CodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class table1
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int i { get; set; }

        [Required]
        [StringLength(20)]
        public string col1 { get; set; }

        [StringLength(20)]
        public string col2 { get; set; }
    }
}
