using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace Exam_Test.Models
{
    public partial class CustomerModel : DbContext
    {
        public CustomerModel()
            : base("name=CustomerModel")
        {
        }

        public virtual DbSet<Customers> Customers { get; set; }
        public virtual DbSet<Goods> Goods { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customers>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<Customers>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<Customers>()
                .Property(e => e.Phone_number)
                .IsUnicode(false);

            modelBuilder.Entity<Customers>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<Goods>()
                .Property(e => e.GoodsName)
                .IsUnicode(false);
        }
    }
}
