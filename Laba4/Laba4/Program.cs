﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba4
{
    class Program
    {
        public enum Orientations
        {
            Left = 0,    // 0001
            Right = 1,   // 0010
            Top = 2,     // 0100
            Bottom = 3   // 1000
        };

        static void testOutMethod(out int helperCount)
        {
            helperCount = 0;
            helperCount++;
            Console.WriteLine("Кiлькiсть помiчникiв в testOutMethod = {0}", helperCount);
        }

        static void testOutMethod2( int helperCount2)
        {
            helperCount2 = 0;
            helperCount2++;
            Console.WriteLine("Кiлькiсть помiчникiв в testOutMethod = {0}", helperCount2);
        }

        static void testRefMethod(ref int recruitCount)
        {
            recruitCount++;
            Console.WriteLine("Кiлькiсть рекрутiв в testRefMethod = {0}", recruitCount);
        }

        static void testRefMethod2( int recruitCount2)
        {
            recruitCount2++;
            Console.WriteLine("Кiлькiсть рекрутiв в testRefMethod = {0}", recruitCount2);
        }
        static void Main(string[] args)
        {
            englishHorseman PeteDonohue = new englishHorseman();
            PeteDonohue.horsemanAge = 28;
            PeteDonohue.horsemanName = "Pete George Donohue";
            PeteDonohue.horsemanRank = "Corporal";
            PeteDonohue.horseName = "Boucefal";
            PeteDonohue.setWifeName("Clara");
            PeteDonohue.sayHello();
            PeteDonohue.getOnHorse();
            PeteDonohue.ride();
            PeteDonohue.talkAboutWife();


            Helper JoshuaSmith = new Helper();
            JoshuaSmith.helperTraining = trainedLevel.Low;

            Recruit JohnathanLawson = new Recruit();
            JohnathanLawson.recruitTraining = trainedLevel.Medium;

            if (JoshuaSmith.helperTraining == trainedLevel.Low && JohnathanLawson.recruitTraining == trainedLevel.Low)
            {
                Console.WriteLine("Things are pretty bad :/ ");
            };

            if (JoshuaSmith.helperTraining != trainedLevel.Low || JohnathanLawson.recruitTraining != trainedLevel.Low)
            {
                Console.WriteLine("Maybe someone will help you. Maybe.");
            }

            Console.WriteLine((Orientations.Left | Orientations.Bottom) &
                     (Orientations.Right | Orientations.Top));

            Recruit JamesWebb = new Recruit();
            JamesWebb.sayHello();
            JamesWebb.toTrain();

            //Archer JosephDuke = new Archer();

            frenchArcher JoanDassault = new frenchArcher(80.0);
            //JoanDassault.actualSalary = 
            Console.WriteLine(JoanDassault.actualSalary);
            Console.WriteLine(JoanDassault.defaultSalary);

            int helperCount;
            testOutMethod(out helperCount); //значення буде 1
            Console.WriteLine("Кiлькiсть помiчникiв в Main = {0}", helperCount); //значення буде 1

            int helperCount2 = 0;
            testOutMethod2(helperCount2); //значення буде 1
            Console.WriteLine("Кiлькiсть помiчникiв в Main = {0}", helperCount2); //значення буде 0

            int recruitCount = 1;
            testRefMethod(ref recruitCount); //значення буде 2
            Console.WriteLine("Кiлькiсть рекрутiв в Main = {0}", recruitCount); //значення буде 2

            int recruitCount2 = 1;
            testRefMethod2( recruitCount2); //значення буде 2
            Console.WriteLine("Кiлькiсть рекрутiв в Main = {0}", recruitCount2); //значення буде 1

            int numberOfSoldiers = 200;
            // boxing 
            object objOfSoldiers = numberOfSoldiers;
            numberOfSoldiers = 100;
            System.Console.WriteLine("Value - type value of numberOfSoldiers is : {0}", numberOfSoldiers);
            System.Console.WriteLine("Object - type value of objOfSoldiers is : {0}", objOfSoldiers);

            // unboxing 
            int numberOfSoldiers2 = (int)objOfSoldiers;
            //Console.WriteLine("Value of objOfSoldiers object is : " + objOfSoldiers);
            Console.WriteLine("Value of numberOfSoldiers2 is : " + numberOfSoldiers2);

            double d = 5673.74;
            int i;
            // cast double to int.
            i = (int)d;
            Console.WriteLine("Number of soldiers before battle: "+i);

            long j = i;
            Console.WriteLine("Number of soldiers before battle: " + j+ " "+j.GetType());

            Console.WriteLine("\n");

            Derived demo = new Derived();
        }  
    }
}
