﻿using System.Collections.Generic;
using System.Net.Http;
using System.Web.Mvc;
using Test_Exam_MVC.Models;

namespace Test_Exam_MVC.Controllers
{
    public class GoodsController : Controller
    {
        // GET: Goods
        public ActionResult Index()
        {
            IEnumerable<Goods> modelList;
            HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Goods").Result;
            modelList = response.Content.ReadAsAsync<IEnumerable<Goods>>().Result;
            return View(modelList);
        }
        
        public ActionResult AddOrEdit(int id = 0)
        {
            if (id == 0)
                return View(new Goods());
            else
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.GetAsync("Goods/" + id.ToString()).Result;
                return View(response.Content.ReadAsAsync<Goods>().Result);
            }
        }

        [HttpPost]
        public ActionResult AddOrEdit(Goods gds)
        {
            if (gds.GoodsID == 0)
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PostAsJsonAsync("Goods", gds).Result;
                TempData["SuccessMessage"] = "Saved Successfully";
            }
            else
            {
                HttpResponseMessage response = GlobalVariables.WebApiClient.PutAsJsonAsync("Goods/" + gds.GoodsID, gds).Result;
                TempData["SuccessMessage"] = "Updated Successfully";
            }
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            HttpResponseMessage response = GlobalVariables.WebApiClient.DeleteAsync("Goods/" + id.ToString()).Result;
            TempData["SuccessMessage"] = "Deleted Successfully";
            return RedirectToAction("Index");
        }
    }
}