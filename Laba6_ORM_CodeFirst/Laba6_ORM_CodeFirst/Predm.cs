namespace Laba6_ORM_CodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Predm")]
    public partial class Predm
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Predm()
        {
            MainTable = new HashSet<MainTable>();
        }

        public int PredmID { get; set; }

        [Required]
        [StringLength(30)]
        public string NamePredm { get; set; }

        public int KilkistGodun { get; set; }

        [Required]
        [StringLength(10)]
        public string PredmType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MainTable> MainTable { get; set; }
    }
}
