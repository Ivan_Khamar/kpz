﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Laba4
{
    struct Archer
    {
        public double defaultSalary;
        public double actualSalary;
        public string ArcherName;
        /*public Archer()
        {
            this.defaultSalary = 60;
        }

        public Archer(double actualSalary)
        {
            this.defaultSalary = actualSalary;
        }*/

        //public override string ToString() { }
        public override string ToString()
        {
            //string rez = new String();
            //rez += defaultSalary.ToString();
            //rez += actualSalary.ToString();
            //rez += ArcherName;
            return "Archer: "+ ArcherName+" "+ defaultSalary+" "+ actualSalary;
        }
        public Archer(string ArcherName, double defaultSalary, double actualSalary)
        {
            this.defaultSalary = defaultSalary;
            this.actualSalary = actualSalary;
            this.ArcherName = ArcherName;
        }
    }
}
