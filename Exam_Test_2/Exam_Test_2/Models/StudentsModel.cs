using System.Data.Entity;

namespace Exam_Test_2.Models
{
    public partial class StudentsModel : DbContext
    {
        public StudentsModel()
            : base("name=StudentsModel")
        {
        }

        public virtual DbSet<Students> Students { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Students>()
                .Property(e => e.Name)
                .IsUnicode(false)
                .IsRequired();

            modelBuilder.Entity<Students>()
                .Property(e => e.Surname)
                .IsUnicode(false);

            modelBuilder.Entity<Students>()
                .Property(e => e.StudentCard)
                .IsUnicode(false);
        }
    }
}
