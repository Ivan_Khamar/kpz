namespace Laba6_ORM_CodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Vukl")]
    public partial class Vukl
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vukl()
        {
            Groups = new HashSet<Groups>();
            MainTable = new HashSet<MainTable>();
        }

        public int VuklID { get; set; }

        [Required]
        [StringLength(50)]
        public string NSS { get; set; }

        [Required]
        [StringLength(20)]
        public string Telef { get; set; }

        [Required]
        [StringLength(30)]
        public string Specialnist { get; set; }

        [Required]
        [StringLength(30)]
        public string Posada { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Groups> Groups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MainTable> MainTable { get; set; }
    }
}
