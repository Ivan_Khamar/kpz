﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Laba5.Models
{
    [Serializable]
    public class Event
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public DateTime Date { get; set; }
    }
}
