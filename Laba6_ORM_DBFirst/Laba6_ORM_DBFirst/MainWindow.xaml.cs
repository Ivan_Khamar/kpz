﻿using System.Data;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Windows;

namespace Laba6_ORM_DBFirst
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RozkladEkzEntities rozkladEkzData;
        SqlConnection connection = new SqlConnection("Server=.;Database=RozkladEkz;Trusted_Connection=True;");
        SqlDataAdapter dataAdapter = new SqlDataAdapter();
        int checker = 0;
        int upper = 4;
        public MainWindow()
        {
            InitializeComponent();
            rozkladEkzData = new RozkladEkzEntities();
            rozkladEkzData.Vukl.Load();
            dataGrid.ItemsSource = rozkladEkzData.Vukl.Local.ToBindingList();
            upper = 4;

            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            rozkladEkzData.Dispose();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            dataGrid.Visibility = Visibility.Visible;
            dataGrid.ItemsSource = rozkladEkzData.Vukl.Local.ToBindingList();

            if (checker == 1)
            {
                dataGrid.Columns[0].Visibility = Visibility.Hidden;
                dataGrid.Columns[5].Visibility = Visibility.Hidden;
                dataGrid.Columns[6].Visibility = Visibility.Hidden;
            }
            //dataGrid.Columns[0].Visibility = Visibility.Hidden;
            checker = 1;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Vukl vuklada4 = new Vukl();
            vuklada4.NSS = fullNameTextBox.Text.Trim();
            vuklada4.Telef = phoneNumberTextBox.Text.Trim();
            vuklada4.Specialnist = specialityTextBox.Text.Trim();
            vuklada4.Posada = postTextBox.Text.Trim();

            rozkladEkzData.Vukl.Add(vuklada4);
            rozkladEkzData.SaveChanges();
        }

        private void deleteDataButton_Click(object sender, RoutedEventArgs e)
        {
            Vukl vuklada4 = dataGrid.SelectedItem as Vukl;
            if (vuklada4 != null)
            {
                rozkladEkzData.Vukl.Remove(vuklada4);
                rozkladEkzData.SaveChanges();
            }
            else
            {
                System.Windows.MessageBox.Show("Select row you want to delete.");
            }     
        }

        private void updateDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedItem != null)
            {
                Vukl updatedVuklada4 = (Vukl)dataGrid.SelectedItem;
                updatedVuklada4.NSS = fullNameTextBox.Text.Trim();
                updatedVuklada4.Telef = phoneNumberTextBox.Text.Trim();
                updatedVuklada4.Specialnist = specialityTextBox.Text.Trim();
                updatedVuklada4.Posada = postTextBox.Text.Trim();

                dataGrid.Items.Refresh();

                rozkladEkzData.SaveChanges();
            }
            else
            {
                System.Windows.MessageBox.Show("Select row you want to update.");
            }
        }
        private void showInfoButtonClick_ADO(object sender, RoutedEventArgs e)
        {
            dataGrid.Visibility = Visibility.Visible;

            dataGrid.Columns.Clear();
            dataGrid.AutoGenerateColumns = true;

            string CmdString = "SELECT * FROM Vukl3";
            SqlCommand cmd = new SqlCommand(CmdString, connection);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGrid.ItemsSource = dt.DefaultView;

            dataGrid.Columns[0].Visibility = Visibility.Hidden;
        }
        private void addDataButtonClick_ADO(object sender, RoutedEventArgs e)
        {
            //dataGrid.Columns.Clear();
            dataGrid.Visibility = Visibility.Visible;
            dataGrid.AutoGenerateColumns = true;

            dataAdapter.InsertCommand = new SqlCommand("INSERT INTO Vukl3 VALUES(@NAME, @PHONE, @SPEC, @POST)", connection);
            dataAdapter.InsertCommand.Parameters.Add("@NAME", SqlDbType.VarChar).Value = fullNameTextBox.Text.Trim();
            dataAdapter.InsertCommand.Parameters.Add("@PHONE", SqlDbType.VarChar).Value = phoneNumberTextBox.Text.Trim();
            dataAdapter.InsertCommand.Parameters.Add("@SPEC", SqlDbType.VarChar).Value = specialityTextBox.Text.Trim();
            dataAdapter.InsertCommand.Parameters.Add("@POST", SqlDbType.VarChar).Value = postTextBox.Text.Trim();

            connection.Open();
            dataAdapter.InsertCommand.ExecuteNonQuery();
            connection.Close();

            string CmdString = "SELECT * FROM Vukl3";
            SqlCommand cmd = new SqlCommand(CmdString, connection);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGrid.ItemsSource = dt.DefaultView;

            dataGrid.Columns[0].Visibility = Visibility.Hidden;
            upper++;
        }

        private void updateDataButtonClick_ADO(object sender, RoutedEventArgs e)
        {
            dataGrid.Visibility = Visibility.Visible;
            dataGrid.AutoGenerateColumns = true;

            ////////////////////////////////////////////////////////////////////////////////////////////////

            //update dbo.Vukl set NSS = @newNSS where VuklID = @currentVuklID

            
            //MessageBox.Show(dataGrid.Items.Count.ToString());

            //dataGrid.CanUserDeleteRows = true;
            //DataTable dt = dataGrid.ItemsSource as DataTable;
            //dt.Rows[dt.Rows.Count].Delete();
            ////dataGrid.Items[dataGrid.Items.Count] = null;
            //dataAdapter.Fill(dt);
            //dataGrid.ItemsSource = dt.DefaultView;

            dataAdapter.UpdateCommand = new SqlCommand("UPDATE Vukl3 SET NSS = @NAME, Telef = @PHONE, Specialnist = @SPEC, Posada = @POST WHERE VuklID = @currentVuklID", connection);
            dataAdapter.UpdateCommand.Parameters.Add("@currentVuklID", SqlDbType.Int).Value = (dataGrid.SelectedIndex + upper); //+ dataGrid.Items.Count%10);
            dataAdapter.UpdateCommand.Parameters.Add("@NAME", SqlDbType.VarChar).Value = fullNameTextBox.Text;
            dataAdapter.UpdateCommand.Parameters.Add("@PHONE", SqlDbType.VarChar).Value = phoneNumberTextBox.Text;
            dataAdapter.UpdateCommand.Parameters.Add("@SPEC", SqlDbType.VarChar).Value = specialityTextBox.Text;
            dataAdapter.UpdateCommand.Parameters.Add("@POST", SqlDbType.VarChar).Value = postTextBox.Text;

            connection.Open();
            dataAdapter.UpdateCommand.ExecuteNonQuery();
            connection.Close();

            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            string CmdString = "SELECT * FROM Vukl3";
            SqlCommand cmd = new SqlCommand(CmdString, connection);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt2 = new DataTable();
            sda.Fill(dt2);
            dataGrid.ItemsSource = dt2.DefaultView;

            dataGrid.Columns[0].Visibility = Visibility.Hidden;
            //System.Windows.MessageBox.Show();
        }

        private void deleteDataButtonClick_ADO(object sender, RoutedEventArgs e)
        {
            dataGrid.Visibility = Visibility.Visible;
            dataGrid.AutoGenerateColumns = true;

            //delete FROM Vukl WHERE Vukl.VuklID = @vuklID

            dataAdapter.DeleteCommand = new SqlCommand("DELETE FROM Vukl3 WHERE VuklID = @vuklID", connection);
            dataAdapter.DeleteCommand.Parameters.Add("@vuklID", SqlDbType.Int).Value = (dataGrid.SelectedIndex + 1);

            connection.Open();
            dataAdapter.DeleteCommand.ExecuteNonQuery();
            connection.Close();

            string CmdString = "SELECT * FROM Vukl3";
            SqlCommand cmd = new SqlCommand(CmdString, connection);
            SqlDataAdapter sda = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            dataGrid.ItemsSource = dt.DefaultView;

            dataGrid.Columns[0].Visibility = Visibility.Hidden;
            upper--;
        }
    }
}
