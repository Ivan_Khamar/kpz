﻿using System.Data.Entity;
using System.Windows;

namespace Laba6_ORM_CodeFirst
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RozkladEkzModel rozkladEkzData;
        public MainWindow()
        {
            InitializeComponent();
            rozkladEkzData = new RozkladEkzModel();
            rozkladEkzData.Vukl.Load();
            dataGrid.ItemsSource = rozkladEkzData.Vukl.Local.ToBindingList();

            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            rozkladEkzData.Dispose();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            dataGrid.Visibility = Visibility.Visible;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            Vukl vuklada4 = new Vukl();
            vuklada4.NSS = fullNameTextBox.Text.Trim();
            vuklada4.Telef = phoneNumberTextBox.Text.Trim();
            vuklada4.Specialnist = specialityTextBox.Text.Trim();
            vuklada4.Posada = postTextBox.Text.Trim();

            rozkladEkzData.Vukl.Add(vuklada4);
            //rozkladEkzData.SaveChanges();
        }

        private void updateDataButton_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid.SelectedItem != null)
            {
                Vukl updatedVuklada4 = (Vukl)dataGrid.SelectedItem;
                updatedVuklada4.NSS = fullNameTextBox.Text.Trim();
                updatedVuklada4.Telef = phoneNumberTextBox.Text.Trim();
                updatedVuklada4.Specialnist = specialityTextBox.Text.Trim();
                updatedVuklada4.Posada = postTextBox.Text.Trim();

                dataGrid.Items.Refresh();

                //rozkladEkzData.SaveChanges();
            }
            else
            {
                MessageBox.Show("Select row you want to update.");
            }
        }

        private void deleteDataButton_Click(object sender, RoutedEventArgs e)
        {
            Vukl vuklada4 = dataGrid.SelectedItem as Vukl;
            if (vuklada4 != null)
            {
                rozkladEkzData.Vukl.Remove(vuklada4);

                //rozkladEkzData.SaveChanges();
            }
            else
            {
                MessageBox.Show("Select row you want to delete.");
            }
        }
    }
}
