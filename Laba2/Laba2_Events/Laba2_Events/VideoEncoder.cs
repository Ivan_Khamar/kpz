﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Laba2_Events
{
    public class VideoEventArgs : EventArgs
    {
        public Video Video { get; set; }
    }

    public class VideoEncoder
    {

        public delegate void VideoEncodedEventHandler(object source, VideoEventArgs args);

        public event VideoEncodedEventHandler VideoEncoded;

        public event EventHandler<VideoEventArgs> VideoEncoded2;

        //public event EventHandler VideoEncoded3; 

        public void Encode(Video video, Video video2)
        {
            Console.WriteLine("Encoding video...");
            Thread.Sleep(3000);

            onVideoEncoded(video, video2);
        }

        protected virtual void onVideoEncoded(Video video, Video video2)
        {
            if (VideoEncoded != null)
            {
                Console.WriteLine("\n");
                VideoEncoded(this, new VideoEventArgs(){Video = video});
                Console.WriteLine("\n");
            }

            if (VideoEncoded2 != null)
            {
                VideoEncoded2(this, new VideoEventArgs() { Video = video2 });
                Console.WriteLine("\n");
            }
        }
    }
}
