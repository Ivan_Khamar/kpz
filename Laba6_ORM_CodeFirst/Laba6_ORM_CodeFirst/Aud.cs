namespace Laba6_ORM_CodeFirst
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Aud")]
    public partial class Aud
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Aud()
        {
            MainTable = new HashSet<MainTable>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int AudID { get; set; }

        [Required]
        [StringLength(50)]
        public string NameAud { get; set; }

        public int NumberAud { get; set; }

        public int Korpys { get; set; }

        public int Mistkist { get; set; }

        public int RozmirAud { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MainTable> MainTable { get; set; }
    }
}
