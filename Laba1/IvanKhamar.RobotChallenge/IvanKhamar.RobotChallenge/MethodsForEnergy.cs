﻿using System;
using System.Linq;
using Robot.Common;

namespace IvanKhamar.RobotChallenge
{
    public class MethodsForEnergy 
    {
        static private int MinimumValue(int value1, int value2) 
        {
            int[] numbers =
            {
                (int) Math.Pow(value1 - value2, 2),
                (int) Math.Pow(value1 - value2 + 100, 2),
                (int) Math.Pow(value1 - value2 - 100, 2)
            };
            return numbers.Min();
        }

        static public bool IsEnergyReserveBiggerThanRequested(int energyReserve, int energyNeeded)
        {
            return (energyReserve >= energyNeeded);
        }

        static public int MinimalPassLenght(Position stationCoords, Position robotKhamarCoords)
        {
            return MinimumValue(robotKhamarCoords.X, stationCoords.X) + MinimumValue(robotKhamarCoords.Y, stationCoords.Y);
        }
        static public bool IsEnoughEnergyForRobotCreation(int energyReserve, Robot.Common.Robot robotKhamar)
        {

            if (robotKhamar.Energy > (200 + energyReserve) && (200 + energyReserve) < 800)
            {
                return true;
            }
            return false;
        }
    }
}
