namespace Exam_Test.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Goods
    {
        public int GoodsID { get; set; }

        [Required]
        [StringLength(50)]
        public string GoodsName { get; set; }

        public int GoodsQuan { get; set; }
    }
}
